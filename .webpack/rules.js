const path = require('path');

const { endsWith } = require('lodash');

const {
  babelLoader,
  makeCssLoader,
  makeSassLoader,
  sassVariablesLoader,
  assetsLoader,
  svgReactLoader,
  rawLoader,
} = require('./loaders');

const rootPath = path.resolve(__dirname, '..');

// Transpile using babel.
const makeBabelRule = ({ mode }) => ({
  test: /\.jsx?$/i,
  use: babelLoader,

  exclude: [
    /\.raw\.jsx?$/i,
    mode !== 'production' && /node_modules/i,
  ].filter(Boolean),

  include: mode === 'production' ?
    [
      path.resolve(rootPath, 'src'),
      ...[
        'string-strip-html',
        'ranges-apply',
        'ranges-push',
        'react-dom',
      ].map(mod => path.resolve(rootPath, 'node_modules', mod)),
    ]
    : undefined,
});

const rawRule = {
  test: /(\.raw(\.[^.]+)?|\.example)$/i,
  use: rawLoader,
};

const makeCssRule = ({ mode } = {}) => ({
  test: file => !endsWith(file, '.module.css') && endsWith(file, '.css'),
  use: makeCssLoader({ mode }),
});

const makeCssModulesRule = ({ mode } = {}) => ({
  test: /\.module\.css$/i,
  use: makeCssLoader({ mode, enableModules: true }),
});

const makeSassRule = ({ mode } = {}) => ({
  test: file => (
    !endsWith(file, '.module.scss')
    && !endsWith(file, '.variables.scss')
    && endsWith(file, '.scss')
  ),
  use: makeSassLoader({ mode }),
});

const makeSassModulesRule = ({ mode } = {}) => ({
  test: /\.module\.scss$/i,
  use: makeSassLoader({ mode, enableModules: true }),
});

const sassVariablesRule = {
  test: /\.variables\.scss$/i,
  use: sassVariablesLoader,
};

// Rule for assets file and NODE_MODULES svg.
const assetsRule = {
  test: /(node_modules[\\/].+\.svg)|(\.(jpg|jpeg|bmp|png|gif|eot|otf|ttf|woff|woff2|ico|pdf))$/i,
  use: assetsLoader,
};

// Rule for app svg files.
const svgReactRule = {
  test: /\.svg$/i,
  exclude: /node_modules/i,
  use: svgReactLoader,
};

module.exports = {
  makeBabelRule,
  rawRule,
  makeCssRule,
  makeCssModulesRule,
  makeSassRule,
  makeSassModulesRule,
  sassVariablesRule,
  assetsRule,
  svgReactRule,
};
