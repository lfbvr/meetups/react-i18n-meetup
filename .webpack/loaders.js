const path = require('path');
const fs = require('fs');

const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const pixrem = require('pixrem');

const babelConfig = JSON.parse(fs.readFileSync(
  path.resolve(__dirname, '../.babelrc'),
  { encoding: 'utf8' },
));

const babelLoader = [
  {
    loader: 'babel-loader',
    options: {
      ...babelConfig,
    },
  },
];

const rawLoader = [
  {
    loader: 'raw-loader',
  },
];

const makeCssLoader = ({
  mode = 'development',
  enableModules = false,
  importLoaders = 0,
} = {}) => ([
  {
    loader: mode === 'production' ?
      // In production, extract css to an external stylesheet to allow caching.
      MiniCssExtractPlugin.loader
      // In development, use the style-loader (faster and allows MHR).
      : 'style-loader',
  },
  {
    loader: 'css-loader',
    options: {
      sourceMap: true,
      importLoaders: importLoaders + 1,
      modules: enableModules ?
        {
          localIdentName: mode === 'production' ?
            // In production, only set the hash of the class name.
            '[hash:base64:8]'
            // In development, add the actual class name to the hash to make it easier to debug.
            : '[local]--[hash:base64:8]',
        }
        : undefined,
    },
  },
  {
    loader: 'postcss-loader',
    options: {
      sourceMap: true,
      plugins: [
        pixrem(),
      ],
    },
  },
]);

const makeSassLoader = ({
  mode,
  enableModules,
  importLoaders = 0,
} = {}) => ([
  ...makeCssLoader({
    enableModules,
    mode,
    importLoaders: importLoaders + 2,
  }),
  {
    // This loader will resolve URLs relative to the source file.
    // (otherwise they will be relative to the built destination file and URLs will not work).
    loader: 'resolve-url-loader',
    options: {
      sourceMap: true,
    },
  },
  {
    loader: 'sass-loader',
    options: {
      sourceMap: true,
      includePaths: [
        path.resolve(__dirname, '../src/'),
        path.resolve(__dirname, '../node_modules/'),
      ],
    },
  },
]);

const sassVariablesLoader = [
  {
    loader: 'sass-extract-loader',
    options: {
      plugins: ['sass-extract-js'],
      includePaths: [
        path.resolve(__dirname, '../src/'),
        path.resolve(__dirname, '../node_modules/'),
      ],
    },
  },
];

const assetsLoader = [
  {
    loader: 'file-loader',
    options: {
      name: 'static/media/[name].[hash:8].[ext]',
    },
  },
];

const svgReactLoader = [
  {
    // Export a React component.
    loader: 'svg-react-loader',
  },
  // {
  //   loader: 'image-webpack-loader',
  //   options: {
  //     // Optimize svg files.
  //     svgo: {
  //       plugins: [
  //         // Keep the viewbox and remove the hardcoded dimensions
  //         // to be able to set the dimensions via css.
  //         { removeViewBox: false },
  //         { removeDimensions: true },

  //         // Removing ids breaks some svgs.
  //         { cleanupIDs: false },
  //       ],
  //     },
  //   },
  // },
];

module.exports = {
  babelLoader,
  rawLoader,
  makeCssLoader,
  makeSassLoader,
  sassVariablesLoader,
  assetsLoader,
  svgReactLoader,
};
