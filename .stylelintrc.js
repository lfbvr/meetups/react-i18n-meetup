module.exports = {
  extends: ['stylelint-config-recommended-scss'],

  rules: {
    'selector-pseudo-class-no-unknown': [
      true,
      {
        ignorePseudoClasses: ['global'],
      },
    ],

    'no-descending-specificity': null,
  },
};
