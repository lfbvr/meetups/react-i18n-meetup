const path = require('path');

const webpack = require('webpack');
const chalk = require('chalk');
const { pickBy } = require('lodash');

const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const HtmlReplaceWebpackPlugin = require('html-replace-webpack-plugin');
const ModuleScopePlugin = require('react-dev-utils/ModuleScopePlugin');
const CaseSensitivePathsWebpackPlugin = require('case-sensitive-paths-webpack-plugin');
const WatchMissingNodeModulesPlugin = require('react-dev-utils/WatchMissingNodeModulesPlugin');
const { BundleAnalyzerPlugin } = require('webpack-bundle-analyzer');
const PreloadWebpackPlugin = require('preload-webpack-plugin');
const ScriptExtHtmlWebpackPlugin = require('script-ext-html-webpack-plugin');

const errorOverlayMiddleware = require('react-dev-utils/errorOverlayMiddleware');

const noopServiceWorkerMiddleware = require('react-dev-utils/noopServiceWorkerMiddleware');

const {
  makeBabelRule,
  rawRule,
  makeCssRule,
  makeCssModulesRule,
  makeSassRule,
  makeSassModulesRule,
  sassVariablesRule,
  assetsRule,
  svgReactRule
} = require('./.webpack/rules');

/**
 * Filter environment using the ENV_PREFIX env variable.
 *
 * @returns {object} Filtered env as dict.
 */
function getAppEnvironment() {
  const prefix = (process.env.ENV_PREFIX || '').toLowerCase();
  return pickBy(
    process.env,
    (value, key) => (
      key
        .toLowerCase()
        .startsWith(prefix)
    )
  );
}

module.exports = (env, { mode = 'development', generateReport = false } = {}) => {
  /* eslint-disable no-console */
  console.log(chalk`Building for {cyan.bold ${mode}}`);
  console.log(chalk`• [${generateReport ? '{green.bold ✔}' : ' '}] Bundle analysis report`);
  /* eslint-enable no-console */

  const appEnv = getAppEnvironment();

  // Served from the root by webpack-dev-server in development and nginx in production.
  const publicPath = process.env.APP_BASE_PATH || '/';

  // Point sourcemap entries to original disk location (format as URL on Windows).
  const devtoolModuleFilenameTemplate = (info) => path
    .relative('./src', info.absoluteResourcePath)
    .replace(/\\/g, '/');

  const output = mode === 'production' ? {
    path: path.resolve(__dirname, './.dist/'),
    filename: 'js/[name].[contenthash].min.js',
    chunkFilename: 'static/js/[name].[chunkhash:8].chunk.js',
    publicPath,
    devtoolModuleFilenameTemplate
  } : {
    path: path.resolve(__dirname, './.dev/'),
    filename: 'js/bundle.min.js',
    chunkFilename: 'static/js/[name].chunk.js',
    publicPath,
    devtoolModuleFilenameTemplate
  };

  const extractCssPlugin = new MiniCssExtractPlugin({
    filename: 'css/[name].[contenthash].min.css'
  });

  const htmlPlugin = new HtmlWebpackPlugin({
    filename: 'index.html',
    template: './src/index.html.ejs'
  });

  return {
    mode,

    devtool: 'source-map',

    // App entry point.
    entry: [
      mode === 'development' && require.resolve('react-dev-utils/webpackHotDevClient'),
      './src/index.jsx'
    ].filter(Boolean),

    // Bundle output.
    output,

    optimization: {
      splitChunks: {
        chunks: 'all',
        maxInitialRequests: 3,
        cacheGroups: {
          vendors: {
            test: /[\\/]node_modules[\\/]/,
            priority: -10
          },
          'default': {
            minChunks: 2,
            priority: -20,
            reuseExistingChunk: true
          }
        }
      }
    },

    module: {
      rules: [
        // JavaScript.
        makeBabelRule({ mode: 'production' }),
        rawRule,

        // Sass, less and css.
        makeCssRule({ mode }),
        makeCssModulesRule({ mode }),
        makeSassRule({ mode }),
        makeSassModulesRule({ mode }),
        sassVariablesRule,

        // Assets.
        assetsRule,
        // App svg files (specific loader to have the actual <svg> tag in the DOM).
        svgReactRule,
      ]
    },

    resolve: {
      // Resolve absolute imports using these paths (in this order).
      modules: [
        './src/',
        './node_modules/'
      ],

      extensions: [
        '.json',
        '.js',
        '.jsx'
      ],

      alias: {
        '../utils/prism-import': path.join(__dirname, '.webpack', 'empty.js'),
      },

      plugins: [
        // Prevent importing files outside of src/ (or node_modules/) except package.json.
        new ModuleScopePlugin('./src/', ['./package.json'])
      ]
    },

    plugins: [
      // Extract CSS to an external stylesheet. Better performance in production (allows caching).
      mode === 'production' && extractCssPlugin,

      // Generate index.html linking to the generated bundles (js and css).
      htmlPlugin,

      new ScriptExtHtmlWebpackPlugin({ defaultAttribute: 'defer' }),

      // In production, variables are resolved at runtime by Nginx.
      // Replace variables for other environments at build time.
      // This means that you need to restart webpack when editing the environment.
      mode !== 'production' && new HtmlReplaceWebpackPlugin(
        Object
          .keys(appEnv)
          .map((key) => ({
            pattern: `$${key}`,
            replacement: appEnv[key]
          }))
      ),

      new HtmlReplaceWebpackPlugin([
        {
          pattern: '$APP_BASE_PATH',
          replacement: process.env.APP_BASE_PATH || '/'
        }
      ]),

      // Define specific environment variables in the bundle.
      new webpack.DefinePlugin({
        // process.env.NODE_ENV is required by many packages.
        'process.env.NODE_ENV': JSON.stringify(mode),
        'process.env.ENV_PREFIX': JSON.stringify(process.env.ENV_PREFIX),
        'process.env.npm_package_name': JSON.stringify(process.env.npm_package_name),
        'process.env.npm_package_version': JSON.stringify(process.env.npm_package_version)
      }),

      // HMR plugin.
      mode === 'development' && new webpack.HotModuleReplacementPlugin(),

      // Throw error when a required path does not match the case of the actual path.
      new CaseSensitivePathsWebpackPlugin(),

      // Trigger a new build when a node module package is installed.
      mode === 'development' && new WatchMissingNodeModulesPlugin(path.resolve('./node_modules/')),

      // Ignore all locale files of moment.js
      new webpack.IgnorePlugin(/^\.\/locale$/, /moment$/),

      new PreloadWebpackPlugin({
        rel: 'prefetch',
      }),

      generateReport && new BundleAnalyzerPlugin({
        analyzerMode: 'static',
        reportFilename: path.resolve(path.join(__dirname, '.reports/bundle-analyzer-report.html'))
      })
    ].filter(Boolean),

    // Some libraries import Node modules but don't use them in the browser.
    // Tell Webpack to provide empty mocks for them so importing them works.
    node: {
      dgram: 'empty',
      fs: 'empty',
      net: 'empty',
      tls: 'empty',
      child_process: 'empty'
    },

    // Only show errors.
    stats: 'minimal',

    // Dev server.
    devServer: mode === 'development'
      ? {
        publicPath,

        contentBase: './.dev',
        watchContentBase: true,
        watchOptions: {
          ignored: [/node_modules/, '.jest', '.cache']
        },

        // Only show errors.
        stats: 'minimal',

        // Enable HMR.
        hot: true,
        // Since react-dev-utils now uses native WebSockets, we need to specify explicitly to use
        // WebSockets instead of sock-js.
        transportMode: 'ws',
        injectClient: false,

        // Serve index.html on 404.
        historyApiFallback: {
          // Paths with dots should still use the history fallback.
          // See https://github.com/facebookincubator/create-react-app/issues/387.
          disableDotRule: true
        },

        disableHostCheck: true,
        host: '0.0.0.0',

        /**
         * Add error overlay.
         *
         * @param {object} app - App.
         */
        before(app) {
          // This lets us open files from the runtime error overlay.
          app.use(errorOverlayMiddleware());
          // This service worker file is effectively a 'no-op' that will reset any
          // previous service worker registered for the same host:port combination.
          // We do this in development to avoid hitting the production cache if
          // it used the same host and port.
          // https://github.com/facebookincubator/create-react-app/issues/2272#issuecomment-302832432
          app.use(noopServiceWorkerMiddleware());
        }
      }
      : undefined
  };
};
