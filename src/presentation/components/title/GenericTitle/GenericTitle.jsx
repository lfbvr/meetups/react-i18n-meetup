import React from 'react';
import { Heading } from 'spectacle';
import classNames from './title.module.scss';

export default ({ title }) => (
  <Heading size={1} fit caps lineHeight={1} textColor="quaternary" className={classNames.title}>
    {title}
  </Heading>
);
