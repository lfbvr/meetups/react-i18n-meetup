import React from 'react';
import { Code } from 'spectacle';
import cn from 'classnames';

import classNames from './code-dark.module.scss';

export default ({
  className,
  preClassName,
  codeClassName,
  ...props
}) => (
  <div className={cn(className, classNames.container)}>
    <pre className={cn(preClassName, classNames.pre)}>
      <Code {...props} className={cn(codeClassName, classNames.code)} />
    </pre>
  </div>
);
