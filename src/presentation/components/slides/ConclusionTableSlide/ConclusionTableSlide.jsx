import React from 'react';
import {
  Slide,
  Table,
  TableBody,
  TableRow,
  TableItem,
  Heading,
} from 'spectacle';
import cn from 'classnames';

import classNames from './table.module.scss';

export default ({
  title,
  criterias,
}) => (
  <Slide className={classNames.slide}>
    <Heading className={classNames.title} size={2}>{title}</Heading>

    <Table className={classNames.table}>
      <TableBody>
        {criterias.map(({
          label,
          note,
          difficulty,
        }) => (
          <TableRow key={label}>
            <TableItem>{label}</TableItem>
            <TableItem
              className={cn(
                classNames.note,
                classNames[difficulty],
              )}
            >
              {note}
            </TableItem>
          </TableRow>
        ))}
      </TableBody>
    </Table>
  </Slide>
);
