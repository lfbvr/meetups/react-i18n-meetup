import React from 'react';
import { Slide } from 'spectacle';

import GenericTitle from '../../title/GenericTitle/GenericTitle';

export default ({ title, children }) => (
  <Slide transition={['fade']} bgColor="tertiary">
    <GenericTitle title={title} />
    {
      children
    }
  </Slide>
);
