import { IntlProvider } from 'react-intl';

const LocaleProvider = ({ children }) => {
  const [
    locale,
    setLocale,
  ] = useState(DEFAULT_LOCALE);

  return (
    <LocaleContext.Provider
      value={{ locale, setLocale }}
    >
      <IntlProvider
        locale={locale}
        messages={langs[locale].messages}
      >
        {children}
      </IntlProvider>
    </LocaleContext.Provider>
  );
};

///

import { FormattedMessage } from 'react-intl';

const App = () => (
  <LocaleProvider>
    <LocaleSelector />
    <FormattedMessage id="hello" />
    {' '}
    <FormattedMessage id="world" />
  </LocaleProvider>
);
