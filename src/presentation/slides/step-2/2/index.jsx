import React from 'react';
import CodeSlide from 'spectacle-code-slide';

import example from './code.raw';

export default ({ slideIndex }) => (
  <CodeSlide
    slideIndex={slideIndex}
    transition={[]}
    lang="jsx"
    code={example}
    ranges={[
      { loc: [0, 1], title: 'Add IntlProvider' },
      { loc: [12, 18], title: 'Add IntlProvider' },
      { loc: [29, 32], title: 'Use FormattedMessage' },
    ]}
  />
);
