import React from 'react';
import CodeSlide from 'spectacle-code-slide';

import example from './code.raw';

export default ({ slideIndex }) => (
  <CodeSlide
    slideIndex={slideIndex}
    transition={[]}
    lang="jsx"
    code={example}
    ranges={[
      { loc: [2, 6], title: 'Plural message' },
      { loc: [11, 12], title: 'Pass values' },
      { loc: [20, 23], title: 'Format numbers' },
      { loc: [30, 36], title: 'Format dates' },
      { loc: [38, 39], title: 'Format dates' },
      { loc: [45, 49], title: 'Messages with HTML' },
    ]}
  />
);
