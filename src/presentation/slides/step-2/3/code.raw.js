import { FormattedMessage } from 'react-intl';

const defaultMessage = `I have {count, plural,
                                =0    {no dogs}
                                one   {# dog}
                                other {# dogs}}`;

const DogsCount = ({ count }) => (
  <FormattedMessage
    id="dogsCount"
    defaultMessage={defaultMessage}
    values={{ count }}
  />
);

///

import { FormattedNumber } from 'react-intl';

const Nb = () => (
  <FormattedNumber
    value={1000}
  />
);

///

import { FormattedDate } from 'react-intl';

const BirthDate = () => (
  <FormattedDate
    value="1993-10-05"
    year="numeric"
    month="long"
    day="2-digit"
  />
);

// October 05, 1993

///

import { FormattedHTMLMessage } from 'react-intl';

const Welcome = () => (
  <FormattedHTMLMessage
    id="title"
    defaultMessage="Welcome to <b>Swagshop</b>"
  />
);
