import React from 'react';

import ConclusionTableSlide from 'presentation/components/slides/ConclusionTableSlide/ConclusionTableSlide';

const criterias = [
  {
    label: 'Implementation difficulty',
    note: 'Up-and-running very fast',
    difficulty: 'easy',
  },
  {
    label: 'Scalability to add a new language',
    note: 'By hand',
    difficulty: 'medium',
  },
  {
    label: 'Scalability to update languages',
    note: 'Keep translations up-to-date by hand',
    difficulty: 'hard',
  },
  {
    label: 'Handle variables in messages',
    note: 'ICU message syntax',
    difficulty: 'easy',
  },
  {
    label: 'Integrate translations',
    note: 'Need to merge translators changes by hand',
    difficulty: 'hard',
  },
];

export default () => (
  <ConclusionTableSlide
    title="Developers"
    criterias={criterias}
  />
);
