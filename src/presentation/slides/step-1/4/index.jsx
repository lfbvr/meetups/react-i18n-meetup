import React from 'react';

import ConclusionTableSlide from 'presentation/components/slides/ConclusionTableSlide/ConclusionTableSlide';

const criterias = [
  {
    label: 'Translation difficulty',
    note: 'Need to know JSON format',
    difficulty: 'medium',
  },
  {
    label: 'Message format',
    note: 'No message syntax convention',
    difficulty: 'hard',
  },
];

export default () => (
  <ConclusionTableSlide
    title="Translators"
    criterias={criterias}
  />
);
