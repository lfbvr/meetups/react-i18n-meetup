import React from 'react';
import CodeSlide from 'spectacle-code-slide';

import example from './code.raw';

export default ({ slideIndex }) => (
  <CodeSlide
    slideIndex={slideIndex}
    transition={[]}
    lang="jsx"
    code={example}
    ranges={[
      {
        loc: [2, 16],
        title: 'Define langs',
      },
      {
        loc: [19, 25],
        title: 'Define LocaleContext',
      },
      {
        loc: [28, 42],
        title: 'Define LocaleProvider',
      },
      {
        loc: [45, 54],
        title: 'Define a consumer',
      },
      { loc: [57, 70] },
      { loc: [73, 79] },
    ]}
  />
);
