///

const locales = {
  en: {
    messages: {
      hello: 'Hello',
      world: 'World',
    },
  },
  fr: {
    messages: {
      hello: 'Bonjour',
      world: 'Monde',
    },
  },
};

///

const DEFAULT_LOCALE = 'en';

const LocaleContext = React.createContext({
  locale: DEFAULT_LOCALE,
  setLocale: noop,
});

///

const LocaleProvider = ({ children }) => {
  const [
    locale,
    setLocale,
  ] = useState(DEFAULT_LOCALE);

  return (
    <LocaleContext.Provider
      value={{ locale, setLocale }}
    >
      {children}
    </LocaleContext.Provider>
  );
};

///

const Message = ({ id, defaultMessage }) => (
  <LocaleContext.Consumer>
    {({ locale = DEFAULT_LOCALE }) => (
      locales[locale].messages[id]
      || defaultMessage
      || id
    )}
  </LocaleContext.Consumer>
);

///

const LocaleSelector = () => (
  <LocaleContext.Consumer>
    {({ locale, setLocale }) => (
      <Select
        value={locale}
        onChange={setLocale}
      >
        <option value="en">English</option>
        <option value="fr">Français</option>
      </Select>
    )}
  </LocaleContext.Consumer>
);

///

const App = () => (
  <LocaleProvider>
    <LocaleSelector />
    <Message id="hello" /> <Message id="world" />
  </LocaleProvider>
);
