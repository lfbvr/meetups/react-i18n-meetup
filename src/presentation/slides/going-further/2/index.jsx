import React from 'react';
import CodeSlide from 'spectacle-code-slide';

import example from './code.raw';

export default ({ slideIndex }) => (
  <CodeSlide
    slideIndex={slideIndex}
    transition={[]}
    lang="jsx"
    code={example}
    ranges={[
      { loc: [5, 6], title: 'messages in state' },
      { loc: [12, 18], title: 'Load langs on-the-fly' },
      { loc: [29, 30], title: 'Pass messages' },
    ]}
  />
);
