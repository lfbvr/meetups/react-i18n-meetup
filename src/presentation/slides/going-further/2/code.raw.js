import { IntlProvider } from 'react-intl';

class LocaleProvider extends React.PureComponent {
  state = {
    locale: DEFAULT_LOCALE,
    messages: {},
  };

  componentDidMount() {
    this.setLocale(DEFAULT_LOCALE);
  }

  setLocale = async (locale) => {
    const messages
      = await import(`locales/${locale}.po`);

    this.setState({ locale, messages });
  };

  render() {
    return (
      <LocaleContext.Provider
        value={{
          locale: this.state.locale,
          setLocale: this.setLocale,
        }}
      >
        <IntlProvider
          locale={locale}
          messages={this.state.messages}
        >
          {children}
        </IntlProvider>
      </LocaleContext.Provider>
    );
  }
}
