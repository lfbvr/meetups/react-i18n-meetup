import React from 'react';
import CodeSlide from 'spectacle-code-slide';

import example from './code.raw';

export default ({ slideIndex }) => (
  <CodeSlide
    slideIndex={slideIndex}
    transition={[]}
    lang="jsx"
    code={example}
    ranges={[
      { loc: [0, 15], title: 'Read browser locale' },
      { loc: [18, 26], title: 'Set html lang' },
    ]}
  />
);
