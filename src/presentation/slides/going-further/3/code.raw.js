function getBrowserLocale() {
  // IE
  if (navigator.userLanguage) {
    return navigator.userLanguage.substr(0, 2);
  }

  // Chrome / Firefox / Safari
  if navigator.languages?.length) {
    return navigator.languages[0].substr(0, 2);
  } if (navigator.language) {
    return navigator.language.substr(0, 2);
  }

  return '';
}

///

function setHtmlLangAttribute(locale) {
  const element = document
    .getElementsByTagName('html')?.[0];

  if (element) {
    element.setAttribute('lang', locale);
  }
};
