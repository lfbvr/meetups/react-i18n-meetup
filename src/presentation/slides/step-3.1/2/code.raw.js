module.exports = {
  hooks: {
    'pre-commit': [
      'yarn intl:update',
      'git add src/**/*.po',
    ].join(' && ')
  },
};
