import React from 'react';
import CodeSlide from 'spectacle-code-slide';

import example from './code.raw';

export default ({ slideIndex }) => (
  <CodeSlide
    slideIndex={slideIndex}
    transition={[]}
    lang="js"
    code={example}
    ranges={[
      { loc: [0, 8], title: '.huskyrc.js', note: 'yarn add --dev husky' },
      { loc: [2, 3], title: 'When commiting' },
      { loc: [3, 4], title: 'Sync PO files' },
      { loc: [4, 5], title: 'Add updated PO files' },
    ]}
  />
);
