import React from 'react';
import TitleSlide from 'presentation/components/slides/TitleSlide/TitleSlide';

export default () => (
  <TitleSlide title="Syncing translations files with the sources" />
);
