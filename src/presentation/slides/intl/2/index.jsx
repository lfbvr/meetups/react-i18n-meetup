import React from 'react';
import CodeSlide from 'spectacle-code-slide';

import example from './code.raw';

export default ({ slideIndex }) => (
  <CodeSlide
    slideIndex={slideIndex}
    transition={[]}
    lang="jsx"
    code={example}
    ranges={[
      {
        loc: [0, 5],
        title: 'Format numbers',
      },
      {
        loc: [8, 15],
        title: 'Format dates',
      },
      {
        loc: [18, 34],
        title: 'Get plural categories',
      },
      {
        loc: [37, 54],
        title: 'Get ordinal categories',
      },
    ]}
  />
);
