const frNbFmt = new Intl.NumberFormat('fr-FR');
const enNbFmt = new Intl.NumberFormat('en-GB');

frNbFmt.format(5.53); // -> "5,53"
enNbFmt.format(5.53); // -> "5.53"

///

const frFmt = new Intl.DateTimeFormat('fr-FR');
const enFmt = new Intl.DateTimeFormat('en-GB');
const usFmt = new Intl.DateTimeFormat('en-US');

frFmt.format(new Date()); // -> "22/01/2020"
enFmt.format(new Date()); // -> "22/01/2020"
usFmt.format(new Date()); // -> "01/22/2020"

///

const frPlural = new Intl.PluralRules('fr-FR');
const arPlural = new Intl.PluralRules('ar-AR');

frPlural.select(0);   // -> "one"
frPlural.select(1);   // -> "one"
frPlural.select(1.5); // -> "one"
frPlural.select(2);   // -> "other"
frPlural.select(100); // -> "other"

arPlural.select(0);   // -> "zero"
arPlural.select(1);   // -> "one"
arPlural.select(1.5); // -> "other"
arPlural.select(2);   // -> "two"
arPlural.select(3);   // -> "few"
arPlural.select(11);  // -> "many"
arPlural.select(100); // -> "other"

///

const frOrdinal = new Intl.PluralRules(
  'fr-FR',
  { type: 'ordinal' },
);

frOrdinal.select(1); // -> "one"
frOrdinal.select(2); // -> "other"
frOrdinal.select(3); // -> "other"

const enOrdinal = new Intl.PluralRules(
  'en-GB',
  { type: 'ordinal' },
);

enOrdinal.select(1); // -> "one"
enOrdinal.select(2); // -> "two"
enOrdinal.select(3); // -> "few"
