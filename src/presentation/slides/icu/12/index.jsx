import React from 'react';
import {
  Heading,
  Slide,
  List,
  ListItem,
  Code,
  Text,
  Link,
} from 'spectacle';

export default () => (
  <Slide bgColor="primary">
    <Heading size={3} textColor="tertiary" margin="0 0 2rem">
      plural
    </Heading>

    <List margin="4rem 0 0" style={{ display: 'inline-block' }}>
      <ListItem><Code>zero</Code> is not the same as <Code>=0</Code>.</ListItem>
      <ListItem><Code>one</Code> is not the same as <Code>=1</Code>.</ListItem>
      <ListItem>etc.</ListItem>
    </List>

    <Text margin="4rem 0 0">
      → Keywords are <i>locale specific</i>.
    </Text>

    <Text margin="2rem 0 0">
      → See the <Link target="_blank" href="http://www.unicode.org/cldr/charts/latest/supplemental/language_plural_rules.html">Unicode CLDR</Link>.
    </Text>
  </Slide>
);
