import React from 'react';
import {
  Heading,
  Slide,
  Text,
} from 'spectacle';

import CodeDark from 'presentation/components/code/CodeDark/CodeDark';

import classNames from './slide.module.scss';

import dateFormatter from './date.raw.icu';
import resultFr from './result.fr.raw.icu';
import resultEn from './result.en.raw.icu';

export default () => (
  <Slide bgColor="primary">
    <Heading size={3} textColor="tertiary" margin="0 0 2rem">
      date
    </Heading>

    <div className={classNames.codeContainer}>
      <CodeDark codeClassName={classNames.code}>{dateFormatter}</CodeDark>

      <div>
        <Text textAlign="left">fr-FR</Text>
        <CodeDark codeClassName={classNames.code}>{resultFr}</CodeDark>
      </div>

      <div>
        <Text textAlign="left">en-GB</Text>
        <CodeDark codeClassName={classNames.code}>{resultEn}</CodeDark>
      </div>
    </div>
  </Slide>
);
