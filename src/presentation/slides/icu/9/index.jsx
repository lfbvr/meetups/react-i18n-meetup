import React from 'react';
import {
  Heading,
  Slide,
  Text,
} from 'spectacle';

import CodeDark from 'presentation/components/code/CodeDark/CodeDark';

import classNames from './slide.module.scss';

import selectEn from './select.en.raw.icu';
import selectFr from './select.fr.raw.icu';

export default () => (
  <Slide bgColor="primary">
    <Heading size={3} textColor="tertiary" margin="0 0 2rem">
      select
    </Heading>

    <Text margin="2rem 0"><i><b>select</b></i> works like a <i>switch</i></Text>

    <div className={classNames.resultsContainer}>
      <div>
        <Text textAlign="left">en</Text>
        <CodeDark codeClassName={classNames.code}>{selectEn}</CodeDark>
      </div>

      <div>
        <Text textAlign="left">fr</Text>
        <CodeDark codeClassName={classNames.code}>{selectFr}</CodeDark>
      </div>
    </div>
  </Slide>
);
