import React from 'react';
import {
  Heading,
  Slide,
  Text,
} from 'spectacle';

import CodeDark from 'presentation/components/code/CodeDark/CodeDark';

import pluralEn from './plural.en.raw.icu';
import pluralFr from './plural.fr.raw.icu';
import pluralCz from './plural.cz.raw.icu';

import classNames from './slide.module.scss';

export default () => (
  <Slide bgColor="primary">
    <Heading size={3} textColor="tertiary" margin="0 0 2rem">
      plural
    </Heading>

    <div className={classNames.pluralsContainer}>
      <div>
        <Text textAlign="left">en</Text>
        <CodeDark
          className={classNames.codeContainer}
          preClassName={classNames.pre}
          codeClassName={classNames.code}
        >
          {pluralEn}
        </CodeDark>
      </div>

      <div>
        <Text textAlign="left">fr</Text>
        <CodeDark
          className={classNames.codeContainer}
          preClassName={classNames.pre}
          codeClassName={classNames.code}
        >
          {pluralFr}
        </CodeDark>
      </div>

      <div>
        <Text textAlign="left">cz</Text>
        <CodeDark
          className={classNames.codeContainer}
          preClassName={classNames.pre}
          codeClassName={classNames.code}
        >
          {pluralCz}
        </CodeDark>
      </div>
    </div>
  </Slide>
);
