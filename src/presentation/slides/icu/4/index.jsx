import React from 'react';
import {
  Heading,
  Slide,
  Text,
} from 'spectacle';

export default () => (
  <Slide bgColor="primary">
    <Heading size={3} textColor="tertiary">
      ICU: Disclaimer
    </Heading>

    <Text margin="4rem 0 2rem">
      → Only in C/C++ and Java.
    </Text>
    <Text margin="2rem 0">
      → But implemented in a lot of languages.
    </Text>
  </Slide>
);
