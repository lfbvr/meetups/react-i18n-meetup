import React from 'react';
import {
  Heading,
  Slide,
  Text,
  List,
  ListItem,
  Code,
} from 'spectacle';

export default () => (
  <Slide bgColor="primary">
    <Heading size={3} textColor="tertiary" margin="0 0 4rem">
      plural
    </Heading>

    <Text>Plural <i>format</i> string keywords:</Text>
    <List style={{ display: 'inline-block' }}>
      <ListItem><Code>zero</Code> </ListItem>
      <ListItem><Code>one</Code></ListItem>
      <ListItem><Code>two</Code></ListItem>
      <ListItem><Code>few</Code></ListItem>
      <ListItem><Code>many</Code></ListItem>
      <ListItem><Code>other</Code></ListItem>
      <ListItem><Code>=NB</Code> (e.g. <Code>=0</Code>, <Code>=1</Code>, etc.)</ListItem>
    </List>
  </Slide>
);
