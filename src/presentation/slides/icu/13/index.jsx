import React from 'react';
import {
  Heading,
  Slide,
  Text,
} from 'spectacle';

import CodeDark from 'presentation/components/code/CodeDark/CodeDark';

import selectordinalEn from './selectordinal.en.raw.icu';
import selectordinalFr from './selectordinal.fr.raw.icu';

import classNames from './slide.module.scss';

export default () => (
  <Slide bgColor="primary">
    <Heading size={3} textColor="tertiary" margin="0 0 2rem">
      selectordinal
    </Heading>

    <div className={classNames.pluralsContainer}>
      <div>
        <Text textAlign="left">en</Text>
        <CodeDark
          className={classNames.codeContainer}
          preClassName={classNames.pre}
          codeClassName={classNames.code}
        >
          {selectordinalEn}
        </CodeDark>
      </div>

      <div>
        <Text textAlign="left">fr</Text>
        <CodeDark
          className={classNames.codeContainer}
          preClassName={classNames.pre}
          codeClassName={classNames.code}
        >
          {selectordinalFr}
        </CodeDark>
      </div>
    </div>
  </Slide>
);
