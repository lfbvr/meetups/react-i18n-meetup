import React from 'react';
import {
  Heading,
  Slide,
  List,
  ListItem,
  Code,
} from 'spectacle';

export default () => (
  <Slide bgColor="primary">
    <Heading size={3} textColor="tertiary">
      Arabic
    </Heading>

    <List style={{ display: 'inline-block' }}>
      <ListItem><Code>n == 0</Code></ListItem>
      <ListItem><Code>n == 1</Code></ListItem>
      <ListItem><Code>n == 2</Code></ListItem>
      <ListItem><Code>{'n % 1 == 0 && n % 100 >= 3 && n % 100 <= 10'}</Code></ListItem>
      <ListItem><Code>{'n % 1 == 0 && n % 100 >= 11'}</Code></ListItem>
      <ListItem><Code>rest</Code></ListItem>
    </List>
  </Slide>
);
