import React from 'react';
import {
  Heading,
  Slide,
  List,
  ListItem,
  Code,
} from 'spectacle';

export default () => (
  <Slide bgColor="primary">
    <Heading size={3} textColor="tertiary">
      Czech
    </Heading>

    <List style={{ display: 'inline-block' }}>
      <ListItem><Code>n == 1</Code></ListItem>
      <ListItem><Code>n == 2 || n == 3 || n == 4</Code></ListItem>
      <ListItem><Code>n % 1 != 0</Code></ListItem>
      <ListItem><Code>rest</Code></ListItem>
    </List>
  </Slide>
);
