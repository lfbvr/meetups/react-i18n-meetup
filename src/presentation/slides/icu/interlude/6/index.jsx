import React from 'react';
import {
  Heading,
  Slide,
  Image,
} from 'spectacle';

import tryNotToCry from './try-not-to-cry.gif';

export default () => (
  <Slide bgColor="primary">
    <Heading size={3} textColor="tertiary">
      Russian
    </Heading>

    <Image
      margin="4rem auto"
      width="16rem"
      src={tryNotToCry}
    />
  </Slide>
);
