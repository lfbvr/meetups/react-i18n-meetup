import React from 'react';
import {
  Heading,
  Slide,
  List,
  ListItem,
  Code,
} from 'spectacle';

import one from './one.raw.icu';
import few from './few.raw.icu';
import many from './many.raw.icu';

import classNames from './slide.module.scss';

export default () => (
  <Slide bgColor="primary">
    <Heading size={3} textColor="tertiary">
      Russian
    </Heading>

    <List className={classNames.list}>
      <ListItem><pre><Code>{one}</Code></pre></ListItem>
      <ListItem><pre><Code>{few}</Code></pre></ListItem>
      <ListItem><pre><Code>{many}</Code></pre></ListItem>
      <ListItem><Code>rest</Code></ListItem>
    </List>
  </Slide>
);
