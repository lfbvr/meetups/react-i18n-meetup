import React from 'react';
import {
  Heading,
  Slide,
  List,
  ListItem,
  Code,
} from 'spectacle';

export default () => (
  <Slide bgColor="primary">
    <Heading size={3} textColor="tertiary">
      English
    </Heading>

    <List style={{ display: 'inline-block' }}>
      <ListItem><Code>n == 1</Code></ListItem>
      <ListItem><Code>rest</Code></ListItem>
    </List>
  </Slide>
);
