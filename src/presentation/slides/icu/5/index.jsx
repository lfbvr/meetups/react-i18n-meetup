import React from 'react';
import {
  Heading,
  Slide,
  List,
  ListItem,
} from 'spectacle';

import CodeDark from 'presentation/components/code/CodeDark/CodeDark';

import formatter from './formatter.raw.icu';

export default () => (
  <Slide bgColor="primary">
    <Heading size={3} textColor="tertiary" margin="0 0 2rem">
      Formatters
    </Heading>

    <CodeDark>{formatter}</CodeDark>

    <List>
      <ListItem margin="1rem 0"><i><b>input</b></i> input data.</ListItem>
      <ListItem margin="1rem 0">
        <i><b>type</b></i> (optional) <i>number</i>
        , <i>date</i>, <i>time</i>, <i>select</i>, <i>plural</i>, <i>selectordinal</i>.
      </ListItem>
      <ListItem margin="1rem 0"><i><b>format</b></i> (optional) how to render the <i>input</i> based on <i>type</i>.</ListItem>
    </List>
  </Slide>
);
