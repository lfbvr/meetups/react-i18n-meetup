import React from 'react';
import {
  Heading,
  Slide,
  Text,
} from 'spectacle';

import CodeDark from 'presentation/components/code/CodeDark/CodeDark';

import classNames from './slide.module.scss';

import numberFormatter from './number.raw.icu';
import resultFr from './result.fr.raw.icu';
import resultEn from './result.en.raw.icu';

export default () => (
  <Slide bgColor="primary">
    <Heading size={3} textColor="tertiary" margin="0 0 2rem">
      number
    </Heading>

    <div className={classNames.codeContainer}>
      <CodeDark codeClassName={classNames.code}>{numberFormatter}</CodeDark>

      <div>
        <Text textAlign="left">fr</Text>
        <CodeDark codeClassName={classNames.code}>{resultFr}</CodeDark>
      </div>

      <div>
        <Text textAlign="left">en</Text>
        <CodeDark codeClassName={classNames.code}>{resultEn}</CodeDark>
      </div>
    </div>

    <Text margin="4rem 0 0 0">By default, the input type is not inferred.</Text>
  </Slide>
);
