import React from 'react';
import {
  Heading,
  Slide,
  List,
  ListItem,
  Code,
  Text,
  Link,
} from 'spectacle';

export default () => (
  <Slide bgColor="primary">
    <Heading size={3} textColor="tertiary" margin="0 0 2rem">
      selectordinal
    </Heading>

    <List margin="4rem 0 0" style={{ display: 'inline-block' }}>
      <ListItem>Keywords are the same as in <Code>plural</Code>.</ListItem>
      <ListItem>They do not have the same meaning.</ListItem>
    </List>

    <Text margin="4rem 0 0">
      → Keywords are (still) <i>locale specific</i>.
    </Text>

    <Text margin="2rem 0 0">
      → See the <Link target="_blank" href="http://www.unicode.org/cldr/charts/latest/supplemental/language_plural_rules.html">Unicode CLDR</Link>.
    </Text>
  </Slide>
);
