import React from 'react';
import {
  Heading,
  Slide,
  Text,
} from 'spectacle';

import CodeDark from 'presentation/components/code/CodeDark/CodeDark';

import helloEn from './hello.en.raw.icu';
import helloFr from './hello.fr.raw.icu';
import helloAr from './hello.ar.raw.icu';

import classNames from './slide.module.scss';

export default () => (
  <Slide bgColor="primary">
    <Heading size={3} textColor="tertiary">
      ICU message syntax
    </Heading>

    <Text margin="4rem 0 2rem">
      Defined in the ICU library
    </Text>

    <div className={classNames.codeContainer}>
      <CodeDark>{helloEn}</CodeDark>
      <CodeDark>{helloFr}</CodeDark>
      <CodeDark>{helloAr}</CodeDark>
    </div>
  </Slide>
);
