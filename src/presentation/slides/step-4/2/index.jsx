import React from 'react';
import {
  Heading,
  Slide,
  Image,
  ListItem,
  List,
} from 'spectacle';

import weblateLogo from './weblate-logo.png';

export default () => (
  <Slide bgColor="primary">
    <Heading size={3} textColor="tertiary">
      Weblate
    </Heading>

    <Image src={weblateLogo} width="15rem" margin="2rem auto" />

    <List>
      <ListItem style={{ fontSize: '2.15rem' }}>Open source web application to manage translations.</ListItem>
      <ListItem style={{ fontSize: '2.15rem' }}>Can be synced with a VCS repository.</ListItem>
      <ListItem style={{ fontSize: '2.15rem' }}>Can open pull requests on Github and Gitlab.</ListItem>
    </List>
  </Slide>
);
