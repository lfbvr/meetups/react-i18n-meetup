import React from 'react';

import ConclusionTableSlide from 'presentation/components/slides/ConclusionTableSlide/ConclusionTableSlide';

const criterias = [
  {
    label: 'Translation difficulty',
    note: 'Web application, no need to handle PO files',
    difficulty: 'easy',
  },
  {
    label: 'Message format',
    note: 'Common message syntax, can be complex',
    difficulty: 'medium',
  },
];

export default () => (
  <ConclusionTableSlide
    title="Translators"
    criterias={criterias}
  />
);
