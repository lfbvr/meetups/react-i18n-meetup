import React from 'react';
import {
  Heading,
  Slide,
  List,
  ListItem,
} from 'spectacle';

export default () => (
  <Slide bgColor="primary">
    <Heading size={3} textColor="tertiary" margin="0 0 4rem">
      Pros and Cons
    </Heading>

    <Heading size={4} textAlign="left">
      Cons
    </Heading>

    <List margin="2rem 0 0">
      <ListItem margin="1rem 0">
        Pull requests not available on all VCS.
      </ListItem>
      <ListItem margin="1rem 0">
        <div
          style={{
            display: 'inline-block',
            verticalAlign: 'top',
          }}
        >
          Github/Gitlab service account hard to configure.
        </div>
      </ListItem>
      <ListItem margin="1rem 0">
        UI could be more intuitive.
      </ListItem>
      <ListItem margin="1rem 0">
        No SSO.
      </ListItem>
    </List>
  </Slide>
);
