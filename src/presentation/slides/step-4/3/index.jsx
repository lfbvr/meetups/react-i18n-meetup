import React from 'react';
import {
  Heading,
  Slide,
  Image,
} from 'spectacle';

import dashboard from './dashboard.png';

export default () => (
  <Slide bgColor="primary">
    <Heading size={3} textColor="tertiary">
      Translations summary
    </Heading>

    <div style={{ margin: '4rem auto 0', border: '1px solid #cdd0d3' }}>
      <Image src={dashboard} margin="0" />
    </div>
  </Slide>
);
