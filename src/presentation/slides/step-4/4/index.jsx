import React from 'react';
import {
  Heading,
  Slide,
  List,
  ListItem,
} from 'spectacle';

export default () => (
  <Slide bgColor="primary">
    <Heading size={3} textColor="tertiary" margin="0 0 4rem">
      Pros and Cons
    </Heading>

    <Heading size={4} textAlign="left">
      Pros
    </Heading>

    <List margin="2rem 0 0">
      <ListItem margin="1rem 0">
        Free if on-premise.
      </ListItem>
      <ListItem margin="1rem 0">
        Integration with Github/Gitlab/Bitbucket.
      </ListItem>
      <ListItem margin="1rem 0">
        Advancement dashboard.
      </ListItem>
      <ListItem margin="1rem 0">
        Open source and responsive maintainer.
      </ListItem>
    </List>
  </Slide>
);
