import React from 'react';
import { Heading } from 'spectacle';

import TitleSlide from 'presentation/components/slides/TitleSlide/TitleSlide';

export default () => (
  <TitleSlide title="The Continous Localization part">
    <Heading
      caps
      textAlign="left"
      textColor="primary"
      size={6}
    >
      of the Continuous Localization
    </Heading>
  </TitleSlide>
);
