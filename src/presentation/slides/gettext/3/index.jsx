import React from 'react';
import {
  Heading,
  Slide,
  List,
  ListItem,
} from 'spectacle';

export default () => (
  <Slide bgColor="primary">
    <Heading size={3} textColor="tertiary" margin="0 0 2rem">
      gettext
    </Heading>

    <List margin="4rem 0 0">
      <ListItem>File formats (PO/POT).</ListItem>
      <ListItem>Commands to create new translation files.</ListItem>
      <ListItem>Commands to update translation files.</ListItem>
    </List>
  </Slide>
);
