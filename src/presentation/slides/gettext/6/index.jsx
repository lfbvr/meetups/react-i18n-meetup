import React from 'react';
import {
  Heading,
  Slide,
  Text,
} from 'spectacle';

import CodeDark from 'presentation/components/code/CodeDark/CodeDark';

import msginit from './msginit.raw.sh';

export default () => (
  <Slide bgColor="primary">
    <Heading size={3} textColor="tertiary">
      New translation file
    </Heading>

    <Text textAlign="left" margin="4rem 0">
      Create a translation file for a new language with empty translations:
    </Text>

    <div style={{ marginTop: '4rem' }}>
      <CodeDark>
        {msginit}
      </CodeDark>
    </div>
  </Slide>
);
