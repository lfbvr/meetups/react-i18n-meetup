import React from 'react';
import CodeSlide from 'spectacle-code-slide';

import example from './code.raw.po';

export default ({ slideIndex }) => (
  <CodeSlide
    slideIndex={slideIndex}
    transition={[]}
    lang="bash"
    code={example}
    ranges={[
      { loc: [0, 12], title: 'Header' },
      { loc: [10, 11], title: 'File language' },
      { loc: [11, 12], title: 'Language plurals' },
      { loc: [13, 41], title: 'Messages' },
      { loc: [13, 15], title: 'Message comments' },
      { loc: [15, 17], title: 'Message & translation' },
      { loc: [13, 14], title: 'Developer comment' },
      { loc: [14, 15], title: 'Source location' },
      { loc: [15, 16], title: 'Original string' },
      { loc: [16, 17], title: 'Translation' },
      { loc: [18, 29], title: 'Message context' },
      { loc: [32, 35], title: 'Fuzzy messages' },
      { loc: [36, 41], title: 'Obsolete messages' },
    ]}
  />
);
