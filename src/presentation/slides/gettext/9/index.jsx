import React from 'react';
import {
  Heading,
  Slide,
  List,
  ListItem,
  Text,
} from 'spectacle';

export default () => (
  <Slide bgColor="primary">
    <Heading size={3} textColor="tertiary" margin="0 0 4rem">
      Pros and Cons
    </Heading>

    <Heading size={4} textAlign="left">
      Cons
    </Heading>

    <List margin="2rem 0 0">
      <ListItem style={{ fontSize: '2.25rem' }}>
        Native plural support does not handle decimals.
      </ListItem>
      <ListItem style={{ fontSize: '2.25rem' }}>
        Native message extraction only for C source files.
      </ListItem>
    </List>

    <Text textAlign="left" margin="3rem 0 0"><b>In our case:</b></Text>
    <List margin="1rem 0 0">
      <ListItem style={{ fontSize: '2.25rem' }}>
        No Node implementation.
      </ListItem>
      <ListItem style={{ fontSize: '2.25rem' }}>
        Need to divert the use of some features.
      </ListItem>
    </List>
  </Slide>
);
