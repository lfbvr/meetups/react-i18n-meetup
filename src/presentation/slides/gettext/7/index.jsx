import React from 'react';
import {
  Heading,
  Slide,
  Text,
} from 'spectacle';

import CodeDark from 'presentation/components/code/CodeDark/CodeDark';

import msgmerge from './msgmerge.raw.sh';

export default () => (
  <Slide bgColor="primary">
    <Heading size={3} textColor="tertiary">
      Merge
    </Heading>

    <Text textAlign="left" margin="4rem 0">
      Synchronize a translation file with up-to-date translations:
    </Text>

    <div style={{ marginTop: '4rem' }}>
      <CodeDark>
        {msgmerge}
      </CodeDark>
    </div>
  </Slide>
);
