import React from 'react';
import {
  Heading,
  Slide,
} from 'spectacle';

import Workflow from './gettext-workflow.svg';

export default () => (
  <Slide bgColor="primary">
    <Heading size={3} textColor="tertiary">
      gettext workflow
    </Heading>

    <div style={{ margin: '6rem auto 0' }}>
      <Workflow width="100%" height="auto" />
    </div>
  </Slide>
);
