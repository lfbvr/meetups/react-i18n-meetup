import React from 'react';
import {
  Heading,
  Slide,
  List,
  ListItem,
} from 'spectacle';

export default () => (
  <Slide bgColor="primary">
    <Heading size={3} textColor="tertiary" margin="0 0 4rem">
      Pros and Cons
    </Heading>

    <Heading size={4} textAlign="left">
      Pros
    </Heading>

    <List margin="2rem 0 0">
      <ListItem style={{ fontSize: '2.25rem' }}>
        Create new translation files.
      </ListItem>
      <ListItem style={{ fontSize: '2.25rem' }}>
        Synchronize translation files.
      </ListItem>
      <ListItem style={{ fontSize: '2.25rem' }}>
        Lots of external tools compatible with PO format.
      </ListItem>
    </List>
  </Slide>
);
