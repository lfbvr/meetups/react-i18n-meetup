import React from 'react';
import {
  Heading,
  Slide,
  List,
  ListItem,
} from 'spectacle';

export default () => (
  <Slide bgColor="primary">
    <Heading size={3} textColor="tertiary" margin="0 0 2rem">
      gettext?
    </Heading>

    <List margin="4rem 0 0">
      <ListItem>Conventions for naming directories, files...</ListItem>
      <ListItem>Tools to extract C code messages.</ListItem>
      <ListItem><b>Tools to manage translation files.</b></ListItem>
    </List>
  </Slide>
);
