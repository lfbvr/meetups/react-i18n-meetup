import React from 'react';
import TitleSlide from 'presentation/components/slides/TitleSlide/TitleSlide';

export default () => (
  <TitleSlide title="gettext" />
);
