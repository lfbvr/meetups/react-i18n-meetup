import React from 'react';
import { Heading, Slide } from 'spectacle';

export default () => (
  <Slide transition={['fade']} bgColor="tertiary">
    <Heading size={1} textColor="primary" caps>
      TODO
    </Heading>
  </Slide>
);
