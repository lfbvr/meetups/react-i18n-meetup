import React from 'react';
import { Image } from 'spectacle';

import PreviousSlide from '../5';
import meme from './surprised_pikachu_meme.jpg';
import classNames from './slide.module.scss';

export default () => (
  <PreviousSlide overwriteTransitionDuration={0.001}>
    <div className={classNames.container}>
      <Image src={meme} />
    </div>
  </PreviousSlide>
);
