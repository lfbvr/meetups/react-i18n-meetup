import React from 'react';
import {
  Heading,
  Slide,
  List,
  ListItem,
} from 'spectacle';

export default () => (
  <Slide bgColor="primary">
    <Heading size={4} textColor="tertiary">
      Our need
    </Heading>
    <List bulletStyle="💲">
      <ListItem margin="2rem 0 0 0">We need to be able to reach a maximum of users across multiple countries</ListItem>
      <ListItem margin="2rem 0 0 0">It must not increase development time too much</ListItem>
      <ListItem margin="2rem 0 0 0">Adding and updating languages should be easy even after several months / years of development</ListItem>
    </List>
  </Slide>
);
