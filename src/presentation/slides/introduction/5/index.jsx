import React from 'react';
import {
  BlockQuote,
  Cite,
  Quote,
  Slide,
} from 'spectacle';

import GenericTitle from 'presentation/components/title/GenericTitle/GenericTitle';

export default ({ children, overwriteTransitionDuration }) => (
  <Slide transition={['fade']} transitionDuration={overwriteTransitionDuration} bgColor="secondary" textColor="primary">
    <div>
      <GenericTitle title="The issue" />
      <BlockQuote>
        <Quote>Not everyone speaks the same language.</Quote>
        <Cite margin="10px 0 0 30px">Sherlock Holmes</Cite>
      </BlockQuote>
    </div>
    {
      children
    }
  </Slide>
);
