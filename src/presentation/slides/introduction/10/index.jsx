import React from 'react';
import { Text, Slide, Heading } from 'spectacle';

export default () => (
  <Slide bgColor="primary">
    <Heading size={4} textColor="secondary">There is no standard way of managing an app&apos;s internationalization with React.</Heading>
    <Text margin="10rem 0 0" textColor="secondary">Disclaimer: we are not about to present a standard nor propose one.</Text>
  </Slide>
);
