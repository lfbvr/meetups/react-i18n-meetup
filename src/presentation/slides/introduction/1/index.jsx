import React from 'react';
import { Heading, Slide, Text } from 'spectacle';

import classNames from './slide.module.scss';

export default () => (
  <Slide transition={['zoom']} bgColor="primary">
    <Heading size={1} fit caps lineHeight={1} textColor="secondary" className={classNames.title}>
      Continuous Localization with React
    </Heading>
    <Text margin="10px 0 0" textColor="tertiary" fit bold>
      with Gautier & Maxence LEFEBVRE
    </Text>
    <Text textColor="tertiary" className={classNames.link}>
      <a
        target="_blank"
        rel="noopener noreferrer"
        href="https://bit.ly/rbl-i18n"
      >https://bit.ly/rbl-i18n
      </a>
    </Text>
    <Text textColor="tertiary" className={classNames.code}>
      <a
        target="_blank"
        rel="noopener noreferrer"
        href="https://bit.ly/rbl-i18n-code"
      >https://bit.ly/rbl-i18n-code
      </a>
    </Text>
  </Slide>
);
