import React from 'react';
import { Heading } from 'spectacle';
import TitleSlide from 'presentation/components/slides/TitleSlide/TitleSlide';

export default () => (
  <TitleSlide title="The issue - Part Two">
    <Heading textColor="primary" size={4}>The return of the issue</Heading>
  </TitleSlide>
);
