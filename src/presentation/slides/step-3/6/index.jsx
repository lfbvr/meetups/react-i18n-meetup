import React from 'react';
import CodeSlide from 'spectacle-code-slide';

import example from './code.raw.sh';

export default ({ slideIndex }) => (
  <CodeSlide
    slideIndex={slideIndex}
    transition={[]}
    lang="bash"
    code={example}
    ranges={[
      { loc: [0, 5], title: 'Extract' },
      {
        loc: [4, 5],
        title: 'Set msgctxt to id',
        note: 'This is needed to retrieve the id when parsing the PO back to JSON',
      },
    ]}
  />
);
