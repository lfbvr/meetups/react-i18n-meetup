import React from 'react';
import CodeSlide from 'spectacle-code-slide';

import example from './code.raw.sh';

export default ({ slideIndex }) => (
  <CodeSlide
    slideIndex={slideIndex}
    transition={[]}
    lang="bash"
    code={example}
    ranges={[
      { loc: [0, 4], title: 'Create' },
      { loc: [7, 11], title: 'Or update' },
    ]}
  />
);
