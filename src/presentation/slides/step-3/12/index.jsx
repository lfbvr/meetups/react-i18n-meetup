import React from 'react';
import {
  Heading,
  Slide,
  List,
  ListItem,
} from 'spectacle';

export default () => (
  <Slide bgColor="primary">
    <Heading size={3} textColor="tertiary" margin="0 0 2rem">
      Downsides
    </Heading>

    <List margin="4rem 0 0">
      <ListItem>PO files must be synced manually.</ListItem>
      <ListItem>Divert use of <b>msgctxt</b>.</ListItem>
      <ListItem>Need to write custom scripts.</ListItem>
      <ListItem>String locations are wrong.</ListItem>
    </List>
  </Slide>
);
