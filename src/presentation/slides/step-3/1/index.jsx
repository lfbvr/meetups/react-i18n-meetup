import React from 'react';
import TitleSlide from 'presentation/components/slides/TitleSlide/TitleSlide';

export default () => (
  <TitleSlide title="Adding gettext workflow to our solution" />
);
