import React from 'react';

import ConclusionTableSlide from 'presentation/components/slides/ConclusionTableSlide/ConclusionTableSlide';

const criterias = [
  {
    label: 'Translation difficulty',
    note: 'External tools to update PO files easily',
    difficulty: 'easy',
  },
  {
    label: 'Message format',
    note: 'Common message syntax, can be complex',
    difficulty: 'medium',
  },
];

export default () => (
  <ConclusionTableSlide
    title="Translators"
    criterias={criterias}
  />
);
