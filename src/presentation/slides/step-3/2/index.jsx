import React from 'react';
import {
  Heading,
  Slide,
  Image,
} from 'spectacle';

import workflow from './react-intl-po_workflow.png';

export default () => (
  <Slide bgColor="primary">
    <Heading size={3} textColor="tertiary">
      Workflow overview
    </Heading>

    <Image margin="6rem auto 0" src={workflow} />
  </Slide>
);
