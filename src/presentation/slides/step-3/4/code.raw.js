const {
  extractReactIntl
} = require('extract-react-intl-messages');

const locale = 'en';

async function extractToJSON() {
  const msgs = await extractReactIntl(
    [locale],
    './src/**/*.messages.{js,jsx}',
    { defaultLocale: locale },
  );

  const output = Object
    .keys(extractedMsgs[locale])
    .map(id => ({
      id,
      defaultMessage: msgs[locale][id].message,
    }));

  await fs.writeFile(
    'messages.json',
    JSON.stringify(output),
  );
}

extractToJSON()
  .then(() => process.exit(0))
  .catch(() => process.exit(1));
