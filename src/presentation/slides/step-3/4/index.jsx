import React from 'react';
import CodeSlide from 'spectacle-code-slide';

import example from './code.raw';

export default ({ slideIndex }) => (
  <CodeSlide
    slideIndex={slideIndex}
    transition={[]}
    lang="js"
    code={example}
    ranges={[
      { loc: [0, 3], title: 'Extract messages', note: 'Using a node library' },
      { loc: [7, 12], title: 'Extract messages', note: 'Optimize with file suffix' },
      { loc: [9, 10], title: 'Optimize' },
      { loc: [13, 19], title: 'Transform output' },
      { loc: [20, 24], title: 'Write to file' },
      { loc: [26, 29], title: 'Handle process errors' },
    ]}
  />
);
