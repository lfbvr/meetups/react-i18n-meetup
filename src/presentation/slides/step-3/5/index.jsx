import React from 'react';
import CodeSlide from 'spectacle-code-slide';

import example from './code.raw.example';

export default ({ slideIndex }) => (
  <CodeSlide
    slideIndex={slideIndex}
    transition={[]}
    lang="json"
    code={example}
    ranges={[
      { loc: [0, 10], title: 'messages.json' },
    ]}
  />
);
