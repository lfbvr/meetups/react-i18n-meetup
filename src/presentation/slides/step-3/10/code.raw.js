// In webpack config.
module.exports = {
  module: {
    rules: [
      {
        test: /\.po$/i,
        use: [
          'json-loader',
          'react-intl-po-loader',
        ],
      },
    ],
  },
};
