import React from 'react';
import CodeSlide from 'spectacle-code-slide';

import example from './code.raw.po';

export default ({ slideIndex }) => (
  <CodeSlide
    slideIndex={slideIndex}
    transition={[]}
    lang="bash"
    code={example}
    ranges={[
      { loc: [0, 11], title: 'Translate' },
      { loc: [13, 28], title: 'Translate' },
    ]}
  />
);
