import React from 'react';

import ConclusionTableSlide from 'presentation/components/slides/ConclusionTableSlide/ConclusionTableSlide';

const criterias = [
  {
    label: 'Implementation difficulty',
    note: 'Need to bootstrap some scripts',
    difficulty: 'medium',
  },
  {
    label: 'Scalability to add a new language',
    note: 'Single command',
    difficulty: 'easy',
  },
  {
    label: 'Scalability to update languages',
    note: 'Single command for all languages',
    difficulty: 'easy',
  },
  {
    label: 'Handle variables in messages',
    note: 'ICU message syntax',
    difficulty: 'easy',
  },
  {
    label: 'Integrate translations',
    note: 'Need to merge translators changes by hand',
    difficulty: 'hard',
  },
];

export default () => (
  <ConclusionTableSlide
    title="Developers"
    criterias={criterias}
  />
);
