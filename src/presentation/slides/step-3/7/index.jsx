import React from 'react';
import CodeSlide from 'spectacle-code-slide';

import example from './code.raw.pot';

export default ({ slideIndex }) => (
  <CodeSlide
    slideIndex={slideIndex}
    transition={[]}
    lang="bash"
    code={example}
    ranges={[
      { loc: [0, 7], title: 'messages.pot' },
      { loc: [9, 24], title: 'messages.pot' },
    ]}
  />
);
