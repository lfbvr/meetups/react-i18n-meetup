import React from 'react';
import CodeSlide from 'spectacle-code-slide';

import example from './code.raw';

export default ({ slideIndex }) => (
  <CodeSlide
    slideIndex={slideIndex}
    transition={[]}
    lang="js"
    code={example}
    ranges={[
      { loc: [0, 12], title: 'Declare messages' },
      { loc: [4, 5], title: 'Unique id' },
      {
        loc: [5, 6],
        title: 'Default message',
        note: 'In the app default locale',
      },
    ]}
  />
);
