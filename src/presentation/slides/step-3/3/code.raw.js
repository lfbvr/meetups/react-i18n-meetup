import { defineMessages } from 'react-intl';

const messages = defineMessages({
  HELLO: {
    id: 'hello',
    defaultMessage: 'Hello',
  },
  WORLD: {
    id: 'world',
    defaultMessage: 'World',
  },
});
