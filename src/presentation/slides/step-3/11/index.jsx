import React from 'react';
import CodeSlide from 'spectacle-code-slide';

import example from './code.raw';

export default ({ slideIndex }) => (
  <CodeSlide
    slideIndex={slideIndex}
    transition={[]}
    lang="js"
    code={example}
    ranges={[
      { loc: [0, 11], title: 'Import' },
      { loc: [0, 2], title: 'Directly import po' },
      { loc: [5, 6], title: 'Set locales translations' },
    ]}
  />
);
