// Import React
import React, { lazy, Suspense } from 'react';

// Import Spectacle Core tags
import { Deck } from 'spectacle';

// Import theme
import createTheme from 'spectacle/lib/themes/default';

import slidesImports from './slides';

// Require CSS
require('../../node_modules/normalize.css/normalize.css');

const theme = createTheme(
  {
    primary: 'white',
    secondary: '#1F2022',
    tertiary: '#03A9FC',
    quaternary: '#CECECE',
  },
  {
    primary: 'Montserrat',
    secondary: 'Helvetica',
  },
);

const lazies = slidesImports.map(slideImport => lazy(slideImport));

export default () => (
  <Suspense fallback={<div>Loading...</div>}>
    <Deck
      theme={theme}
      showFullscreenControl={false}
      controls={false}
      progress="bar"
    >
      {/* eslint-disable-next-line react/no-array-index-key */}
      {lazies.map((Component, index) => <Component key={index} />)}
    </Deck>
  </Suspense>
);
