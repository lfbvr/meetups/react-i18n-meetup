import React from 'react';
import ReactDOM from 'react-dom';

import Prism from 'prismjs/components/prism-core';
import 'styles/prism-onedark.css';

// retarded prism
import 'prismjs/components/prism-clike';
import 'prismjs/components/prism-javascript';
import 'prismjs/components/prism-js-extras';
import 'prismjs/components/prism-js-templates';
import 'prismjs/components/prism-bash';
import 'prismjs/components/prism-json';
import 'prismjs/components/prism-css';
import 'prismjs/components/prism-markup';
import 'prismjs/components/prism-jsx';

import Presentation from './presentation';
import './styles/index.scss';

Prism.highlightAll();

ReactDOM.render(
  <Presentation/>,
  document.getElementById('root')
);
