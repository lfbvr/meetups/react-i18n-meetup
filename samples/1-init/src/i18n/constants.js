export const SUPPORTED_LANGS = {
  EN: 'en',
  FR: 'fr',
};

export const SUPPORTED_LANGS_LABELS = {
  [SUPPORTED_LANGS.EN]: 'English',
  [SUPPORTED_LANGS.FR]: 'French',
};

export const DEFAULT_LANG = SUPPORTED_LANGS.EN;
