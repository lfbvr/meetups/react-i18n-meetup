import enMessages from './en/messages';
import frMessages from './fr/messages';
import { SUPPORTED_LANGS } from './constants';

export default {
  [SUPPORTED_LANGS.EN]: {
    messages: enMessages,
  },
  [SUPPORTED_LANGS.FR]: {
    messages: frMessages,
  },
};
