export const setHtmlLangAttribute = lang => {
  const html = document.getElementsByTagName('html')[0];
  html.setAttribute('lang', lang);
};
