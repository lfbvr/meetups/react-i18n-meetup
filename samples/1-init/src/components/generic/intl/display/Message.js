import React from 'react';
import { oneOf, node } from 'prop-types';

import langs from 'i18n';
import { DEFAULT_LANG } from 'i18n/constants';

import IntlContext from '../context';

const Message = ({ id, defaultMessage }) => (
  <IntlContext.Consumer>
    { ({ lang = DEFAULT_LANG }) => langs[lang]?.messages?.[id] || defaultMessage || id }
  </IntlContext.Consumer>
);

Message.displayName = 'Message';
Message.propTypes = {
  id: oneOf(Object.keys(langs.en.messages)).isRequired,
  defaultMessage: node,
};
Message.defaultProps = { defaultMessage: null };

export default Message;
