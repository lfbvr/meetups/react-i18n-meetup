import React, { useCallback } from 'react';
import { string, func } from 'prop-types';
import { noop } from 'lodash';
import cn from 'classnames';

import { DEFAULT_LANG, SUPPORTED_LANGS, SUPPORTED_LANGS_LABELS } from 'i18n/constants';
import { setHtmlLangAttribute } from 'i18n/utils';

import withLangHoc from '../context/hoc';
import classNames from './LangSelector.module.css';

const handleOnChange = (setLang) => event => {
  const lang = event.target.value;
  setHtmlLangAttribute(lang);
  setLang(lang);
};

const LangSelector = ({ lang, setLang, className, ...props }) => {
  const onChange = useCallback(
    handleOnChange(setLang),
    [setLang],
  );

  return (
    <select
      {...props}
      className={cn(className, classNames.langSelector)}
      value={lang}
      onChange={onChange}
    >
      {
        Object.values(SUPPORTED_LANGS).map(
          value => (
            <option
              key={value}
              value={value}
            >
              {SUPPORTED_LANGS_LABELS[value]}
            </option>
          )
        )
      }
    </select>
  );
};

LangSelector.displayName = 'LangSelector';
LangSelector.propTypes = {
  lang: string,
  setLang: func,
  className: string
};
LangSelector.defaultProps = {
  lang: DEFAULT_LANG,
  setLang: noop,
  className: ''
};

export default withLangHoc(LangSelector);
