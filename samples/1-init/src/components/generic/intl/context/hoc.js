import React from 'react';
import IntlContext from './index';

const withLangConsumer = (WrappedComponent) => props => (
  <IntlContext.Consumer>
    {
      context => <WrappedComponent {...context} {...props} />
    }
  </IntlContext.Consumer>
);

export default withLangConsumer;
