import React, { useState, useMemo } from 'react';
import { node } from 'prop-types';
import { DEFAULT_LANG } from 'i18n/constants';

import IntlContext from './index';

const IntlProvider = ({ children }) => {
  const [ lang, setLang ] = useState(DEFAULT_LANG);

  const contextValue = useMemo(
    () => ({ lang, setLang }),
    [ lang, setLang ],
  );

  return (
    <IntlContext.Provider value={contextValue}>
      { children }
    </IntlContext.Provider>
  );
};

IntlProvider.displayName = 'IntlProvider';
IntlProvider.propTypes = { children: node };
IntlProvider.defaultProps = { children: null };

export default IntlProvider;
