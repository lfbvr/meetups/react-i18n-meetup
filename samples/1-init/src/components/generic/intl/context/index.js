import React from 'react';
import { noop } from 'lodash';

import { DEFAULT_LANG } from 'i18n/constants';

export default React.createContext({ lang: DEFAULT_LANG, setLang: noop });
