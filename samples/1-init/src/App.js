import React from 'react';

import Message from './components/generic/intl/display/Message';
import LangSelector from './components/generic/intl/selector/LangSelector';

import classNames from './App.module.css';

const App = () => (
  <div className={classNames.app}>
    <header className={classNames.appHeader}>
      <LangSelector className={classNames.langSelector}/>
      <span className={classNames.helloWorld}>
        <Message id="hello_world" defaultMessage="yoloswag"/>
      </span>
    </header>
  </div>
);

App.displayName = 'App';

export default App;
