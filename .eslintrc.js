module.exports = {
  // Tell eslint not to seek a eslintrc file in parent folders.
  root: true,

  env: {
    es6: true,
    // Define jest globals in .spec.js and .test.js files.
    jest: true,
    // Define browser globals (window, document, etc.).
    browser: true,
  },

  extends: 'airbnb',

  parser: 'babel-eslint',

  rules: {
    // Allow ++ operator.
    'no-plusplus': 'off',
    // Allow named export without default export.
    'import/prefer-default-export': 'off',

    // On linebreak, enforce operator on the new line, except for the '?' of a ternary expression.
    'operator-linebreak': ['error', 'before'],

    // Require parenthesis around arrow functions only if multiple args or body.
    'arrow-parens': [
      'error',
      'as-needed',
      {
        requireForBlockBody: true,
      },
    ],

    // Prevent multiple empty lines. Allow 1 at EOF, 0 at BOF.
    'no-multiple-empty-lines': [
      'error',
      {
        max: 1,
        maxEOF: 1,
        maxBOF: 0,
      },
    ],

    // Enforce single quotes except for strings with single quotes in body.
    quotes: [
      'error',
      'single',
      {
        avoidEscape: true,
      },
    ],

    // Allow assigning in argument if object.
    'no-param-reassign': [
      'error',
      {
        props: false,
      },
    ],

    // Enforce one empty line at the end of the file.
    'eol-last': [
      'error',
      'always',
    ],

    // Always declare the state as a class property.
    'react/state-in-constructor': [
      'error',
      'never',
    ],

    // This is not working with defaultProps.
    // https://github.com/yannickcr/eslint-plugin-react/issues/1846
    'react/button-has-type': 'off',

    // Disable missing prop types for the presentation.
    'react/prop-types': 'off',

    // This rule just makes the code more verbose and less readable.
    'react/destructuring-assignment': 'off',
    // Does not work well with inline text.
    'react/jsx-one-expression-per-line': 'off',
    // Allow props spreading.
    'react/jsx-props-no-spreading': 'off',

    'import/no-cycle': 'off',
  },

  settings: {
    'import/resolver': {
      node: {
        extensions: [
          '.js',
          '.jsx',
        ],

        paths: [
          './src/',
        ],
      },
    },
  },

  overrides: [
    {
      files: [
        'scripts/**/*.js',
        'src/**/*.stories.{js,jsx}',
        '.{jest,storybook,webpack}/**/*.{js,jsx}',
      ],
      rules: {
        'import/no-extraneous-dependencies': [
          'error',
          { devDependencies: true },
        ],
      },
    },

    {
      files: [
        '.storybook/**/*.{js,jsx}',
      ],
      rules: {
        'react/jsx-filename-extension': 'off',
      },
    },
  ],
};
